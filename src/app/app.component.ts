import { Component, OnInit } from '@angular/core';
import { PlayersService } from './player/players.service';

@Component({
    selector: 'app-root',
    // moduleId: module.id,
    template: require('./app.component.html')
})
export class AppComponent implements OnInit {

    private keysPressed = {
        left: false,
        right: false,
        up: false,
        down: false,
        space: false
    };

    private leftCancellation: any;
    private rightCancellation: any;
    private upCancellation: any;
    private downCancellation: any;
    private spaceCancellation: any;

    constructor(private playersService: PlayersService) {
    }

    ngOnInit(): void {
        this.playersService.init();

        document.body.addEventListener('keydown', (event) => {
            const keyCode = event.which;

            if (keyCode === 37) {
                this.keysPressed.left = true;
            } else if (keyCode === 39) {
                this.keysPressed.right = true;
            } else if (keyCode === 40) {
                this.keysPressed.down = true;
            } else if (keyCode === 38) {
                this.keysPressed.up = true;
            } else if (keyCode === 32) {
                this.keysPressed.space = true;
                // this.playersService.fireRocket();
            }

            if (this.keysPressed.left && !this.leftCancellation) {
                this.leftCancellation = setInterval(() => {
                    this.playersService.left();
                }, 10);
            }
            if (this.keysPressed.right && !this.rightCancellation) {
                this.rightCancellation = setInterval(() => {
                    this.playersService.right();
                }, 10);
            }
            if (this.keysPressed.down && !this.downCancellation) {
                this.downCancellation = setInterval(() => {
                    this.playersService.back();
                }, 10);
            }
            if (this.keysPressed.up && !this.upCancellation) {
                this.upCancellation = setInterval(() => {
                    this.playersService.forward();
                }, 10);
            }
            if (this.keysPressed.space && !this.spaceCancellation) {
                this.playersService.fireRocket();
                this.spaceCancellation = setInterval(() => {
                    this.playersService.fireRocket();
                }, 300);
            }
        });

        document.body.addEventListener('keyup', (event) => {
            const keyCode = event.which;

            if (keyCode === 37) {
                this.keysPressed.left = false;
                clearInterval(this.leftCancellation);
                this.leftCancellation = undefined;
            } else if (keyCode === 39) {
                this.keysPressed.right = false;
                clearInterval(this.rightCancellation);
                this.rightCancellation = undefined;
            } else if (keyCode === 40) {
                this.keysPressed.down = false;
                clearInterval(this.downCancellation);
                this.downCancellation = undefined;
            } else if (keyCode === 38) {
                this.keysPressed.up = false;
                clearInterval(this.upCancellation);
                this.upCancellation = undefined;
            } else if (keyCode === 32) {
                this.keysPressed.space = false;
                clearInterval(this.spaceCancellation);
                this.spaceCancellation = undefined;
            }
        });

        window.onbeforeunload = (event) => {
            this.playersService.exit();
        };
    }
}
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { CommandType } from '../player/command.type.model';
import { Player } from '../player/player.model';
import { Rocket } from '../player/rocket.model';

@Injectable()
export class ApplicationTransportService {

    private sessionId: string;
    private socket: WebSocket;

    constructor(private http: Http) {
    }

    public initSession(): string {
        this.sessionId = Math.random().toString();

        this.socket = new WebSocket('ws://localhost:8081/');
        this.socket.onopen = (e: Event) => {
            this.socket.send(this.sessionId);
        }

        return this.sessionId;
    }

    public sendPlayerLocationUpdate(player: Player) {
        this.socket.send(JSON.stringify({
            player: ApplicationTransportService.toTransportModel(player),
            command: {
                type: CommandType.PlayerLocationUpdate
            }
        }));
    }

    public sendRocketFiredByPlayer(player: Player, rocket: Rocket) {
        this.socket.send(JSON.stringify({
            player: ApplicationTransportService.toTransportModel(player),
            rocket: {
                id: rocket.id,
                x: rocket.x,
                y: rocket.y,
                angle: rocket.angle,
            },
            command: {
                type: CommandType.RocketFired
            }
        }));
    }

    public sendPlayerDestroyed(player: Player) {
        this.socket.send(JSON.stringify({
            player: ApplicationTransportService.toTransportModel(player),
            command: {
                type: CommandType.PlayerDestroyed
            }
        }));
    }

    public sendPlayerExit(player: Player) {
        this.socket.send(JSON.stringify({
            player: ApplicationTransportService.toTransportModel(player),
            command: {
                type: CommandType.PlayerExit
            }
        }));
    }

    public onServerUpdate(callback: Function) {
        this.socket.onmessage = m => {
            callback(JSON.parse(m.data))
        }
    }

    private static toTransportModel(player: Player): any{
        return {
            sessionId: player.getId(),
            x: player.getX(),
            y: player.getY(),
            angle: player.getA(),
            alive: player.isAlive()
        }
    }
}
import { Injectable } from '@angular/core';
import { Player } from '../player/player.model';
import { Rocket } from '../player/rocket.model';
import { SessionHolder } from './session.holder';

@Injectable()
export class RenderingService {

    private canvas: HTMLElement;

    constructor() {}

    public renderPlayer(player: Player) {
        this.canvas = document.getElementById('canvas');

        let playerId = 'player-' + player.getId();
        let playerElement = document.getElementById(playerId);

        if (!playerElement) {
            playerElement = document.createElement('img');
            playerElement.setAttribute('id', playerId);
            playerElement.style.position = 'absolute';
            this.canvas.appendChild(playerElement);
        }
        playerElement.style.left = player.getX() + 'px';
        playerElement.style.top = player.getY() + 'px';
        playerElement['src'] = '/nave.png';
        playerElement['height'] = '20';
        playerElement['width'] = '20';
        playerElement.style.transform = `rotate(${this.getCssDeg(player)}deg)`;
        playerElement.innerText = player.isAlive() ? '|-<(^)>-|' : '   ><   ';
        this.canvas.appendChild(playerElement);

        const playerName = document.createElement('div');
        playerName.setAttribute('id', playerId + '-name');
        playerName.style.color = SessionHolder.SessionId === player.getId() ? 'green' : 'red';
        playerName.style.textAlign = 'center';
        playerName.style.fontSize = '9px';
        playerName.innerText = player.getId().slice(-7);
        playerElement.appendChild(playerName);

        // const cross = document.createElement('div');
        // const x = player.getCenterX();
        // const y = player.getY();
        // cross.style.left = x + 'px';
        // cross.style.top = y + 'px';
        // cross.style.color = 'red';
        // cross.style.position = 'absolute';
        // cross.innerText = '+';
        // this.canvas.appendChild(cross);
    }

    private getCssDeg(player: Player) {
        let angle = player.getA();

        // 359 = 91 deg
        // 0 = 90deg
        // 1 = 89deg
        if(angle >= 0 && angle < 90){
            return 90 - angle;
        }

        // 270 = 180deg
        // 271 = 179deg
        // 359 = 90eg
        if(angle >= 270 && angle < 360){
            return 180 - (angle - 270);
        }

        // 90 = 0deg
        // 91 = -1deg
        // 89 = 1deg
        if(angle >= 90 && angle < 270){
            return 90 - angle;
        }

        return angle;
    }

    public renderRocket(rocket: Rocket) {
        this.canvas = document.getElementById('canvas');

        const rocketId = 'rocket-' + rocket.id;
        let rocketElement = document.getElementById(rocketId);

        if (!rocketElement) {
            rocketElement = document.createElement('div');
            rocketElement.setAttribute('id', rocketId);
            rocketElement.style.position = 'absolute';
            rocketElement.innerText = '*';
            this.canvas.appendChild(rocketElement);
        }
        rocketElement.style.left = rocket.x + 'px';
        rocketElement.style.top = rocket.y + 'px';
    }

    public renderRocketExplosion(rocket: Rocket) {
        this.canvas = document.getElementById('canvas');
        const rocketId = 'rocket-' + rocket.id;
        const rocketElement = document.getElementById(rocketId);
        rocketElement.innerText = '🗯';
        setTimeout(() => {
            this.canvas.removeChild(rocketElement);
        }, 75);
    }

    public removePlayer(player: Player) {
        this.canvas = document.getElementById('canvas');
        let rocketId = 'player-' + player.getId();
        let rocketElement = document.getElementById(rocketId);
        this.canvas.removeChild(rocketElement);
    }
}
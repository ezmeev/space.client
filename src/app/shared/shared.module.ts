import {NgModule} from "@angular/core";
import {ApplicationTransportService} from "./application-transport.service";
import { PlayersService } from '../player/players.service';
import { RenderingService } from './rendering.service';

@NgModule({
    providers: [ApplicationTransportService, PlayersService, RenderingService]
})
export class SharedModule {

}
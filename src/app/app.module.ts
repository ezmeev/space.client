import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
    imports: [BrowserModule, CommonModule, AppRoutingModule, FormsModule, HttpModule, SharedModule],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule {

}
import { Injectable } from '@angular/core';
import { Player } from './player.model';
import { ApplicationTransportService } from '../shared/application-transport.service';
import { RenderingService } from '../shared/rendering.service';
import { PlayerUpdate } from './player-update.model';
import { RocketUpdate } from './rocket-update.model';
import { Rocket } from './rocket.model';
import { CommandType } from './command.type.model';
import { PlayerState } from './player-state.model';
import { SessionHolder } from '../shared/session.holder';

@Injectable()
export class PlayersService {

    private otherPlayers: Player[] = [];
    private thisPlayer: Player;
    private handlersMap = {};

    constructor(private applicationTransport: ApplicationTransportService, private renderingService: RenderingService) {
        this.handlersMap[CommandType.RocketLocationUpdate] = this.handleRocketLocationUpdate.bind(this);
        this.handlersMap[CommandType.RocketExplosion] = this.handleRocketExplosion.bind(this);
        this.handlersMap[CommandType.RocketFired] = this.handleRocketLocationUpdate.bind(this);

        this.handlersMap[CommandType.PlayerLocationUpdate] = this.handlePlayerLocationUpdate.bind(this);
        this.handlersMap[CommandType.ExistingPlayer] = this.handleNewPlayer.bind(this);
        this.handlersMap[CommandType.NewPlayer] = this.handleNewPlayer.bind(this);
        this.handlersMap[CommandType.PlayerDestroyed] = this.handlePlayerLocationUpdate.bind(this);
        this.handlersMap[CommandType.PlayerExit] = this.handlePlayerExit.bind(this);
    }

    public init(): Player {
        let sessionId = this.applicationTransport.initSession();
        this.applicationTransport.onServerUpdate((m: any) => this.onServerUpdate(m));

        SessionHolder.SessionId = sessionId;

        this.thisPlayer = new Player(new PlayerState(sessionId, -50, -50, 90, true));
        this.renderingService.renderPlayer(this.thisPlayer);

        return this.thisPlayer;
    }

    public left() {
        this.thisPlayer.left();
        this.renderingService.renderPlayer(this.thisPlayer);
        this.applicationTransport.sendPlayerLocationUpdate(this.thisPlayer);
    }

    public right() {
        this.thisPlayer.right();
        this.renderingService.renderPlayer(this.thisPlayer);
        this.applicationTransport.sendPlayerLocationUpdate(this.thisPlayer);
    }

    public back() {
        this.thisPlayer.back();
        this.renderingService.renderPlayer(this.thisPlayer);
        this.applicationTransport.sendPlayerLocationUpdate(this.thisPlayer);
    }

    public forward() {
        this.thisPlayer.forward();
        this.renderingService.renderPlayer(this.thisPlayer);
        this.applicationTransport.sendPlayerLocationUpdate(this.thisPlayer);
    }

    public fireRocket() {
        const rocket = new Rocket(Math.random().toString(), this.thisPlayer.getCenterX(), this.thisPlayer.getY(), this.thisPlayer.getA());
        this.renderingService.renderRocket(rocket);
        this.applicationTransport.sendRocketFiredByPlayer(this.thisPlayer, rocket);
    }

    private onServerUpdate(update: PlayerUpdate) {
        if (update.command) {
            this.handlersMap[update.command.type](update);
        } else {
            // console.log('Update received:', update);
        }
    }

    private handlePlayerLocationUpdate(update: PlayerUpdate) {
        // console.log('Update update location: ', update.player);
        if (this.thisPlayer.getX() < 0 || this.thisPlayer.getId() !== update.player.sessionId) {
            this.renderingService.renderPlayer(new Player(update.player));
        }
    }

    private handlePlayerExit(update: PlayerUpdate) {
        // console.log('Update update location: ', update.player);
        this.renderingService.removePlayer(new Player(update.player));
    }

    private handleRocketLocationUpdate(update: RocketUpdate) {
        // console.log('Update rocket location: ', update.rocket);
        this.renderingService.renderRocket(update.rocket);

        if (update.player.sessionId !== this.thisPlayer.getId()) {
            const x1 = update.rocket.x;
            const y1 = update.rocket.y;
            const x2 = this.thisPlayer.getCenterX();
            const y2 = this.thisPlayer.getY();

            const distance = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
            if (distance < 20) {
                this.thisPlayer.kill();
                this.renderingService.renderPlayer(this.thisPlayer);
                this.applicationTransport.sendPlayerDestroyed(this.thisPlayer);

                setTimeout(() => {
                    this.thisPlayer.revive();
                    this.renderingService.renderPlayer(this.thisPlayer);
                    this.applicationTransport.sendPlayerLocationUpdate(this.thisPlayer);
                }, 3000);
            }
        }
    }

    private handleRocketExplosion(update: RocketUpdate) {
        // console.log('Rocket explosion: ', update.rocket);
        this.renderingService.renderRocketExplosion(update.rocket);
    }

    private handleNewPlayer(update: PlayerUpdate) {
        // console.log('Add new update: ', update);
        const playerState = update.player;
        if (playerState.sessionId === this.thisPlayer.getId()) {
            this.thisPlayer = new Player(playerState);
        } else {
            this.otherPlayers.push(new Player(playerState));
        }
        this.renderingService.renderPlayer(new Player(playerState));
    }

    public exit() {
        this.applicationTransport.sendPlayerExit(this.thisPlayer);
        this.renderingService.removePlayer(this.thisPlayer);
    }
}
export class PlayerState {

    constructor(public sessionId: string, public x: number, public y: number, public angle: number, public alive: boolean) {
    }
}
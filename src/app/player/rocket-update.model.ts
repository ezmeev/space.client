import { Rocket } from './rocket.model';
import { PlayerUpdate } from './player-update.model';

export class RocketUpdate extends PlayerUpdate {
    public rocket: Rocket;
}
export class Rocket {

    constructor(public id: string, public x: number, public y: number, public angle: number) {
    }
}
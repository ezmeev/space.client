export class CommandType {
    public static PlayerLocationUpdate = 'PlayerLocationUpdate';
    public static RocketFired = 'RocketFired';
    public static RocketLocationUpdate = 'RocketLocationUpdate';
    public static RocketExplosion = 'RocketExplosion';
    public static NewPlayer = 'NewPlayer';
    public static ExistingPlayer = 'ExistingPlayer';
    public static PlayerDestroyed = 'PlayerDestroyed';
    public static PlayerExit = 'PlayerExit';
}
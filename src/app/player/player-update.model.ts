import { Player } from './player.model';
import { Command } from './command.model';
import { PlayerState } from './player-state.model';

export class PlayerUpdate{

    public player: PlayerState;
    public command: Command;
}
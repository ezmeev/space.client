import { PlayerState } from './player-state.model';

export class Player {

    private delta = 3;
    private state: PlayerState;

    constructor(state: PlayerState) {
        this.state = state
    }

    public getX() {
        return this.state.x;
    }

    public getCenterX() {
        return this.state.x + 22;
    }

    public getY() {
        return this.state.y;
    }

    public getA() {
        return this.state.angle;
    }

    public getId() {
        return this.state.sessionId;
    }

    public left() {
        if (!this.isAlive())
            return;

        let newAngle = this.state.angle + this.delta * 0.5;
        if (newAngle >= 360)
            newAngle -= 360;
        this.state.angle = newAngle;
    }

    public right() {
        if (!this.isAlive())
            return;

        let newAngle = this.state.angle - this.delta * 0.5;
        if (newAngle < 0)
            newAngle += 360;
        this.state.angle = newAngle;
    }

    public back() {
        if (!this.isAlive())
            return;
        this.state.x -= this.calcDeltaX();
        this.state.y += this.calcDeltaY();
    }

    public forward() {
        if (!this.isAlive())
            return;
        this.state.x += this.calcDeltaX();
        this.state.y -= this.calcDeltaY();
    }

    public kill() {
        this.state.alive = false;
    }

    public revive() {
        this.state.alive = true;
    }

    public isAlive() {
        return this.state.alive;
    }

    private calcDeltaY() {
        return this.delta * Math.sin(this.state.angle * Math.PI / 180.0);
    }

    private calcDeltaX() {
        return this.delta * Math.cos(this.state.angle * Math.PI / 180.0);
    }
}
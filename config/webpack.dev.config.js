var baseConfig = require("./webpack.base.config");
var webpackMerge = require("webpack-merge");

module.exports = webpackMerge(baseConfig, {
    devtool: "eval"
});